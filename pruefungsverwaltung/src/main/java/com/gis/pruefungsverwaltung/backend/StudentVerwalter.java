package com.gis.pruefungsverwaltung.backend;

import java.util.Collection;

import javax.persistence.ElementCollection;

public class StudentVerwalter {
	
	@ElementCollection
	Collection<Student> studenten;

	public StudentVerwalter() {
		super();
	}

	public Collection<Student> getStudenten() {
		return studenten;
	}

	public void setStudenten(Collection<Student> studenten) {
		this.studenten = studenten;
	}
	
	

}
