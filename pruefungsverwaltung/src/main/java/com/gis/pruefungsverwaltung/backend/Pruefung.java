package com.gis.pruefungsverwaltung.backend;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Pruefung {
	
	String name;
	String pruefer;
	@Id
	int pruefungsnummer;
	public Pruefung() {
		super();
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPruefer() {
		return pruefer;
	}
	public void setPruefer(String pruefer) {
		this.pruefer = pruefer;
	}
	public int getPruefungsnummer() {
		return pruefungsnummer;
	}
	public void setPruefungsnummer(int pruefungsnummer) {
		this.pruefungsnummer = pruefungsnummer;
	}
	
	

}
