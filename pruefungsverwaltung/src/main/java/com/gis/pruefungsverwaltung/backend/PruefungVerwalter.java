package com.gis.pruefungsverwaltung.backend;

import java.util.Collection;

import javax.persistence.ElementCollection;

public class PruefungVerwalter {
	
	@ElementCollection
	Collection<Pruefung> pruefungen;

	public PruefungVerwalter() {
		super();
	}

	public Collection<Pruefung> getPruefungen() {
		return pruefungen;
	}

	public void setPruefungen(Collection<Pruefung> pruefungen) {
		this.pruefungen = pruefungen;
	}
	
	

}
