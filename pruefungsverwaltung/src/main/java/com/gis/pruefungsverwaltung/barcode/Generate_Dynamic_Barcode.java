package com.gis.pruefungsverwaltung.barcode;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import com.gis.pruefungsverwaltung.barcode.Barcode_Image;
import com.gis.pruefungsverwaltung.barcode.Barcode_PDF;

public class Generate_Dynamic_Barcode {
	public static void main(String[] args) {
		 	PreparedStatement ps=null;
			Connection connection=null;
			DBConnection obj_DBConnection=new DBConnection();
			connection=obj_DBConnection.getConnection();
			ResultSet rs=null;
		try { 
			String query="select * from studPruef";
			ps = connection.prepareStatement(query);
			rs=ps.executeQuery();
			while(rs.next()){
				Barcode_Image.createImage(rs.getString("barcode")+".png", rs.getString("barcode"));
				Barcode_PDF.createPDF(rs.getString("barcode")+".pdf", rs.getString("barcode"));
				System.out.println("Creating Barcode for "+rs.getString("barcode"));
			}
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			if(connection!=null){
				try {
						connection.close();
					}
				 catch (Exception e2) {
					e2.printStackTrace();
				}
			}
			if(ps!=null){
				try {
					ps.close();
					}
				 catch (Exception e2) {
					 e2.printStackTrace();
				}
			}
		}
		}	
}

