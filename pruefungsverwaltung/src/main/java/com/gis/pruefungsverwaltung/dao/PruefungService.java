package com.gis.pruefungsverwaltung.dao;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;

import com.gis.pruefungsverwaltung.backend.Pruefung;

public class PruefungService {

	private static PruefungService instance;
	private static final Logger LOGGER = Logger.getLogger(PruefungService.class.getName());

	private EntityManager em;
	EntityManagerFactory emf = Persistence.createEntityManagerFactory("Pruefungen");

	private PruefungService() {

		emf = Persistence.createEntityManagerFactory("test");
		em = emf.createEntityManager();
	}

	public static PruefungService getInstance() {
		if (instance == null) {
			instance = new PruefungService();

			try {

			} catch (Exception e) {

			}
		}
		return instance;
	}

	public synchronized void addPruefung(Pruefung entry) {
		if (entry == null) {
			LOGGER.log(Level.SEVERE, "Pruefung is null in PruefungService");
			return;
		}

		em.getTransaction().begin();
		em.persist(entry);
		em.getTransaction().commit();
	}

	public List<Pruefung> readPruefungFromDatabase() {

		TypedQuery<Pruefung> query = em.createQuery("SELECT h FROM Pruefung h", Pruefung.class);

		List<Pruefung> pruefungen = query.getResultList();
		return pruefungen;
	}

	public void updatePruefung(Pruefung pruefung) {

		boolean isManaged = em.contains(pruefung);

		if (isManaged) {
			System.out.println("Pruefung is managed");
			em.getTransaction().begin();
			em.merge(pruefung);
			em.getTransaction().commit();
		}
	}

	public void deletePruefung(Pruefung pruefung) {

		if (pruefung != null) {
			if (em.contains(pruefung)) {
				em.getTransaction().begin();
				em.remove(pruefung);
				em.getTransaction().commit();
			}
		}
	}

	public void close() {
		em.close();
		emf.close();
	}

	public void open() {

		emf = Persistence.createEntityManagerFactory("test");
		em = emf.createEntityManager();

	}

}