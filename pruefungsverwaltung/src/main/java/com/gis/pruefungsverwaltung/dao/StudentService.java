package com.gis.pruefungsverwaltung.dao;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;

import com.gis.pruefungsverwaltung.backend.Student;

public class StudentService {

	private static StudentService instance;
	private static final Logger LOGGER = Logger.getLogger(StudentService.class.getName());

	private EntityManager em;
	EntityManagerFactory emf = Persistence.createEntityManagerFactory("Studenten");

	private StudentService() {

		emf = Persistence.createEntityManagerFactory("test");
		em = emf.createEntityManager();
	}

	public static StudentService getInstance() {
		if (instance == null) {
			instance = new StudentService();

			try {

			} catch (Exception e) {

			}
		}
		return instance;
	}

	public synchronized void addStudent(Student entry) {
		if (entry == null) {
			LOGGER.log(Level.SEVERE, "Student is null in StudentService");
			return;
		}

		em.getTransaction().begin();
		em.persist(entry);
		em.getTransaction().commit();
	}

	public List<Student> readStudentsFromDatabase() {

		TypedQuery<Student> query = em.createQuery("SELECT h FROM Student h", Student.class);

		List<Student> students = query.getResultList();
		return students;
	}

	public void updateStudent(Student student) {

		boolean isManaged = em.contains(student);

		if (isManaged) {
			System.out.println("Student is managed");
			em.getTransaction().begin();
			em.merge(student);
			em.getTransaction().commit();
		}
	}

	public void deleteStudent(Student student) {

		if (student != null) {
			if (em.contains(student)) {
				em.getTransaction().begin();
				em.remove(student);
				em.getTransaction().commit();
			}
		}
	}

	public void close() {
		em.close();
		emf.close();
	}

	public void open() {

		emf = Persistence.createEntityManagerFactory("test");
		em = emf.createEntityManager();

	}

}
