package de.hsosnabrueck.gis.demo_vaadin_app.gui;

import com.vaadin.data.Binder;
import com.vaadin.event.ShortcutAction.KeyCode;
import com.vaadin.ui.Button;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;

import de.hsosnabrueck.gis.demo_vaadin_app.View;
import de.hsosnabrueck.gis.demo_vaadin_app.entity.Customer;
import de.hsosnabrueck.gis.demo_vaadin_app.entity.CustomerService;


/**
 * Simple Customer view for creating new customers.
 * Uses a CustomerService for saving Customers.
 * 
 * @author cge
 *
 */
public class CustomerView extends CustomComponent implements View {
	
	private VerticalLayout mainlayout = new VerticalLayout();

	private Label cusNumber = new Label("Kundennummer: ");
	private Label cusNumberValue = new Label();
	private TextField firstname = new TextField("Vorname");
	private TextField familyname = new TextField("Nachname");
	private TextField street = new TextField("Strasse");
	private TextField zipcode = new TextField("PLZ");
	private TextField place = new TextField("Ort");
	private TextField phone = new TextField("Telefon");
	private TextField email = new TextField("E-Mail");
	
	private Button cancel = new Button("Abbrechen");
	private Button save = new Button("Speichern");
	
	
	private CustomerService service = CustomerService.getInstance();
	private Customer customer = new Customer();
	
	private Binder<Customer> binder = new Binder<>(Customer.class);
	private View endView;
	
	public CustomerView(View view) {
		
		endView = view;
		setSizeUndefined();
		this.addStyleName("v-scrollable");
	    this.setHeight("100%");
		
	    HorizontalLayout customerNumber = new HorizontalLayout(cusNumber, cusNumberValue);
		HorizontalLayout buttons = new HorizontalLayout(cancel, save);
		
		
		mainlayout.addComponents(customerNumber,firstname,familyname, street, zipcode, place, phone, email, buttons); 
		
		mainlayout.setSizeFull();
		//mainlayout.addComponents(layout,buttons);
		
		save.setStyleName(ValoTheme.BUTTON_PRIMARY);
		save.setClickShortcut(KeyCode.ENTER);
		
		configureBinder();
		
		save.addClickListener(e -> addCustomer());
		cancel.addClickListener(e -> finished());
		
		setCompositionRoot(mainlayout);
		
	}

	private void configureBinder() {
		binder.bindInstanceFields(this);
	}
	
	public void show()
	{
		this.setVisible(true);
			
	}
	
	
	public void setCustomer(Customer customer)
	{
		this.customer=customer;
		binder.setBean(customer);
		
		this.cusNumberValue.setValue(customer.getNumber());
		
		setVisible(true);
		familyname.selectAll();
		
	}
	
	private void finished() {
		this.setVisible(false);
		endView.show();

		
	}
	
	private void addCustomer() {
		try {
			binder.writeBean(this.customer);
			service.addCustomer(customer);
		}
		catch (Exception e)
		{
			System.out.println(e.toString());
		}

		this.setVisible(false);
		endView.show();

	}
		

}
