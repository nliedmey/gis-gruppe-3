package de.hsosnabrueck.gis.demo_vaadin_app.gui;

import java.util.List;

import com.vaadin.ui.Button;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.Grid;
import com.vaadin.ui.VerticalLayout;

import de.hsosnabrueck.gis.demo_vaadin_app.View;
import de.hsosnabrueck.gis.demo_vaadin_app.entity.Customer;
import de.hsosnabrueck.gis.demo_vaadin_app.entity.CustomerService;

/**
 * A view for a customer list.
 *
 * @author cge
 */

public class CustomerListView extends CustomComponent implements View {

	private VerticalLayout rootlayout;
	
	protected Grid<Customer> grid = new Grid<>();
	
	private CustomerService service = CustomerService.getInstance();
	private Customer customer;

	private View endView;
	private CustomerView changeView;
	
	private Button finished = new Button("Zurück");
	
	public CustomerListView(View endview) {
		
		rootlayout = new VerticalLayout();
		rootlayout.setSizeFull();
		this.endView=endview;
		
		configureGrid();
			
		updateList();
		
		rootlayout.addComponents(grid,finished);
		setCompositionRoot(rootlayout);
		
		configureChangeView();
		
		finished.addClickListener(e -> finished());
	}


	private void configureGrid() {
		grid.setSizeFull();
		grid.addColumn(Customer::getFirstname).setCaption("Vorname");
		grid.addColumn(Customer::getFamilyname).setCaption("Nachname");
		grid.addColumn(Customer::getStreet).setCaption("Strasse");
		grid.addColumn(Customer::getZipcode).setCaption("PLZ");
		grid.addColumn(Customer::getPlace).setCaption("Ort");
	}
	
	
	public Customer getCustomer() {
		return customer;
	}

	private void configureChangeView() {
		changeView = new CustomerView(this);
		grid.asSingleSelect().addValueChangeListener( event -> {
			if (event.getValue()==null) {
				changeView.setVisible(false);
			} else {
				System.out.println("Selected");
				rootlayout.addComponent(changeView);
				changeView.setCustomer(event.getValue());
				changeView.setVisible(true);
			}
		});
	}

	
	public void updateList() {
		List<Customer> customers = service.readCustomersFromDatabase();
	    grid.setItems(customers);
	
	    if (customers.size()>0)
	    {
	    	grid.setHeightByRows(customers.size());
	    }
	    else grid.setHeightByRows(1);
	}


	@Override
	public void show() {
		this.setVisible(true);
		
	}
	
	private void finished() {
		this.endView.show();
	}
	
	

}