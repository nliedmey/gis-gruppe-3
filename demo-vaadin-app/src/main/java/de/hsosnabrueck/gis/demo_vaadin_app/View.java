package de.hsosnabrueck.gis.demo_vaadin_app;

public interface View {
	public void show();
}
