package de.hsosnabrueck.gis.demo_vaadin_app.entity;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;

/**
 * An in memory dummy "database" for the example purposes. In a typical Java app
 * this class would be replaced by e.g. EJB or a Spring based service class.
 * <p>
 * In demos/tutorials/examples, get a reference to this service class with
 * {@link CustomerService#getInstance()}.
 */
public class CustomerService {

	private static CustomerService instance;
	private static final Logger LOGGER = Logger.getLogger(CustomerService.class.getName());

	private long nextId = 0;
	
	private EntityManager em;
	private EntityManagerFactory emf;

	private CustomerService() {
	
		emf = Persistence.createEntityManagerFactory("test");
	    em = emf.createEntityManager();
		
	}

	/**
	 * @return a reference to an example facade for Customer objects.
	 */
	public static CustomerService getInstance() {
		if (instance == null) {
			instance = new CustomerService();
		
			try {
				
			}
			catch (Exception e)
			{
				
			}
		}
		return instance;
	}

	

	
	
	/**
	 * Persists or updates customer in the system. Also assigns an identifier
	 * for new Customer instances.
	 *
	 * @param entry
	 */
	public synchronized void addCustomer(Customer entry) {
		if (entry == null) {
			LOGGER.log(Level.SEVERE,
					"Customer is null in CustomerService");
			return;
		}
	
		em.getTransaction().begin();
		em.persist(entry);
		em.getTransaction().commit();
	}

	
	
	public List<Customer> readCustomersFromDatabase()
	{
	
		 TypedQuery<Customer> query = em.createQuery("SELECT h FROM Customer h", Customer.class);
		 
		 List<Customer> customers = query.getResultList();
		 return customers;
	}

	public void updateCustomer(Customer customer) {
		
		boolean isManaged = em.contains(customer);
		
		if (isManaged)
		{
			System.out.println("Customer is managed");
			em.getTransaction().begin();
			em.merge(customer);
			em.getTransaction().commit();
		}
	}
	
	
	public void deleteCustomer(Customer customer) {

		if (customer != null)
		{		
			if (em.contains(customer))
			{
				em.getTransaction().begin();
				em.remove(customer);
				em.getTransaction().commit();
			}
		}
	}
	
	public void close()
	{
		em.close();
		emf.close();
	}
	
	public void open()
	{
		
		emf = Persistence.createEntityManagerFactory("test");
	    em = emf.createEntityManager();
		
	}
	

	
	
	
	
	
	

}
