package de.hsosnabrueck.gis.demo_vaadin_app.gui;


import com.vaadin.ui.Button;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.VerticalLayout;

import de.hsosnabrueck.gis.demo_vaadin_app.View;


/**
 * Die MainMenuView ist die View, die als erstes erscheint.
 * 
 * @author cge
 *
 */


public class MainMenuView extends CustomComponent implements View {

	private Button addCustomer = new Button("Kunden anlegen");
	private Button showCustomers = new Button("Kundenliste anzeigen");
	
	CustomerView customerView;
	CustomerListView customerListView;
	
	VerticalLayout buttons = new VerticalLayout(addCustomer, showCustomers);
	
	
	public MainMenuView() {
		
		setSizeFull();
		
		setCompositionRoot(buttons);
		
		addCustomer.addClickListener(e -> addCustomer());
		showCustomers.addClickListener(e -> showCustomerList());

	}

	
	public void show() {
		setCompositionRoot(buttons);
	}

	private void addCustomer() {
		
		customerView = new CustomerView(this);
		customerView.setVisible(true);
		setCompositionRoot(customerView);
		
	}
	
	private void showCustomerList() {
		
		customerListView = new CustomerListView(this);
		customerListView.setVisible(true);
		setCompositionRoot(customerListView);
		
	}
}
	
	
	

